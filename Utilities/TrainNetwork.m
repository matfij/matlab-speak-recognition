clc
close all

try
    load('wordsTrainingData.mat')
    load('lettersTrainingData.mat')
end

wordsTargetData = [zeros(1, 20) ones(1, 20) 2*ones(1, 20)];
lettersTargetData = [zeros(1, 30) ones(1, 30) 2*ones(1, 30)];

Structure = [6 2 6];
ann = newff(wordsTrainingData, wordsTargetData, Structure);

ann.trainParam.epochs = 128; 
ann.trainParam.max_fail = 12;
    
% for i = 1 : 10
%     [ann, Tr] = train(ann, wordsTrainingData, wordsTargetData); 
% 
%     out(i, :) = sim(ann, wordsTrainingData);
%     hold on
%     plot(out(i, :))
% end

for i = 1 : 10
    [ann, Tr] = train(ann, lettersTrainingData, lettersTargetData); 

    out(i, :) = sim(ann, lettersTrainingData);
    hold on
    plot(out(i, :))
end