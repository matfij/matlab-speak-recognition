%function RecordData()
clc
close all

load('letters.mat')
no = length(letters);

fs = 44100;
dt = 1/fs;
tm = 4;
t = dt : dt : tm;
f = 0 : (1/tm) : fs;

for i = 1 : no
    
    letters(i).A = abs(fft(letters(i).a));
    letters(i).A = letters(i).A(1:5000);
   
    subplot(2, 2, 1)
    hold on
    plot(letters(i).A)
    title('A')
    
    letters(i).I = abs(fft(letters(i).i));
    letters(i).I = letters(i).I(1:5000);
    
    subplot(2, 2, 2)
    hold on
    plot(letters(i).I)
    title('I')
    
    letters(i).U = abs(fft(letters(i).u));
    letters(i).U = letters(i).U(1:5000);
    
    subplot(2, 2, 3)
    hold on
    plot(letters(i).U)
    title('U')
end


for i = 1 : no
    features(i).Ax = (sum(letters(i).A(400:1500)) ) / sum(letters(i).A(2500:3000));
    features(i).Ix = (sum(letters(i).I(400:1500)) ) / sum(letters(i).I(2500:3000));
    features(i).Ux = (sum(letters(i).U(400:1500)) ) / sum(letters(i).U(2500:3000));
    
    features(i).Ay = 8*(sum(letters(i).A(4000:5000)) ) / sum(letters(i).A(1500:2300));
    features(i).Iy = 8*(sum(letters(i).I(4000:5000)) ) / sum(letters(i).I(1500:2300));
    features(i).Uy = 8*(sum(letters(i).U(4000:5000)) ) / sum(letters(i).U(1500:2300));
    
    subplot(2, 2, 4)
    hold on
    plot(features(i).Ax ,features(i).Ay, 'ro')
    hold on
    plot(features(i).Ix, features(i).Iy, 'go')
    hold on
    plot(features(i).Ux, features(i).Uy, 'bo')
end

title('Letter Classification')
xlabel('')
ylabel('')
legend('A', 'I', 'U')

%% clustering

aX = 0; aY = 0;
iX = 0; iY = 0;
uX = 0; uY = 0;

for i = 1 : no
    
    aX = aX + features(i).Ax;
    aY = aY + features(i).Ay;
    iX = iX + features(i).Ix;
    iY = iY + features(i).Iy;
    uX = uX + features(i).Ux;
    uY = uY + features(i).Uy;
end

R = 0.60;
catR = R * max(max([features.Ax]) - min([features.Ax]), max([features.Ay]) - min([features.Ay]));
dogR = R * max(max([features.Ix]) - min([features.Ix]), max([features.Iy]) - min([features.Iy]));
sifhR = R * max(max([features.Ux]) - min([features.Ux]), max([features.Uy]) - min([features.Uy]));

subplot(2, 2, 4)
hold on
viscircles([aX/no, aY/no], catR, 'Color', 'r')
hold on
viscircles([iX/no, iY/no], dogR, 'Color', 'g')
hold on
viscircles([uX/no, uY/no], sifhR, 'Color', 'b')


