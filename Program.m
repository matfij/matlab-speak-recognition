function varargout = Program(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Program_OpeningFcn, ...
                   'gui_OutputFcn',  @Program_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
addpath UIControls
addpath ReferenceData
addpath UIControls/Classifiers
function Program_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
set(handles.btnSpeak, 'visible', 'off')
function varargout = Program_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function btnStartLetters_Callback(hObject, eventdata, handles)
    global state
    state = get(handles.workState, 'Value');
    if (state ~= 0)
        state = 1;
    end
    
    while true
      ClassifyLetter(handles)
      if (state ~= 1)
         Clear(handles)
         break
      end
    end
    

function btnStartWords_Callback(hObject, eventdata, handles)
    global state
    state = get(handles.workState, 'Value');
    if (state ~= 0)
        state = 2;
    end
    
    while true
      ClassifyWord(handles)
      if (state ~= 2)
        Clear(handles)
        break
      end
    end


% --- Executes on button press in btnStop.
function btnStop_Callback(hObject, eventdata, handles)
    global state
    state = 0;
    Clear(handles)
