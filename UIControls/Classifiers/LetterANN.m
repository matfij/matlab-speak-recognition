function LetterANN(handles, fx, fy)

ann = load('LettersAnnStructure.mat');

out = sim(ann.ann, [fx; fy]);

certanity = 0;

if (out < .5)
    set(handles.textA, 'ForegroundColor', 'green');
    certanity = (1 - abs(out))/1;
elseif(out < 1.5)
    set(handles.textI, 'ForegroundColor', 'green');
    certanity = out/1;
else
    set(handles.textU, 'ForegroundColor', 'green');
    certanity = (1 - abs(2 - out))/1;
end

certanity = 100*round(certanity, 4);

set(handles.textCertanity, 'String', num2str(certanity));