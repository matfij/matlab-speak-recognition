function WordClustering(handles, fx, fy)

in = [fx fy];

catCenter = [147.8 9.8];
dogCenter = [184.3 84.4];
fishCenter = [1018.1 17.4];

catDist = pdist([in; catCenter], 'euclidean');
dogDist = pdist([in; dogCenter], 'euclidean');
fishDist = pdist([in; fishCenter], 'euclidean');

catdog = pdist([catCenter; dogCenter], 'euclidean');
catfish = pdist([catCenter; fishCenter], 'euclidean');
dogfish = pdist([dogCenter; fishCenter], 'euclidean');

certanity = 0;

if (catDist < dogDist && catDist < fishDist)
    
    if(dogDist < fishDist)
        certanity = catdog/dogDist;
    else
         certanity = catfish/fishDist;
    end
    set(handles.textKot, 'ForegroundColor', 'green');
    
elseif (dogDist < catDist && dogDist < fishDist)
    
    if(catDist < fishDist)
        certanity = catdog/catDist;
    else
         certanity = dogfish/fishDist;
    end
    set(handles.textPies, 'ForegroundColor', 'green');
    
else 
    
    if(catDist < dogDist)
        certanity = catfish/catDist;
    else
         certanity = dogfish/dogDist;
    end
    set(handles.textRyba, 'ForegroundColor', 'green');
    
end

certanity = 100*round(certanity, 4);

set(handles.textCertanity, 'String', strcat(num2str(certanity), '%'));

