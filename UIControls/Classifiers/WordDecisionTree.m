function WordDecisionTree(handles, fx, fy)

if (fx < 600 && fy < 30)
    set(handles.textKot, 'ForegroundColor', 'green');
elseif (fx < 450 && fy > 50)
    set(handles.textPies, 'ForegroundColor', 'green');
else 
    set(handles.textRyba, 'ForegroundColor', 'green');
end