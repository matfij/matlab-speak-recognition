function WordANN(handles, fx, fy)

ann = load('annStructure.mat');

out = sim(ann.ann, [fx; fy]);

certanity = 0;

if (out < .5)
    set(handles.textKot, 'ForegroundColor', 'green');
    certanity = (1 - abs(out))/1;
elseif(out < 1.5)
    set(handles.textPies, 'ForegroundColor', 'green');
    certanity = out/1;
else
    set(handles.textRyba, 'ForegroundColor', 'green');
    certanity = (1 - abs(2 - out))/1;
end

certanity = 100*round(certanity, 4);

set(handles.textCertanity, 'String', strcat(num2str(certanity), ''));