function LettersClustering(handles, fx, fy)

in = [fx fy];

aCenter = [3.23 3.69];
iCenter = [48.21 0.49];
uCenter = [2.27 0.15];

aDist = pdist([in; aCenter], 'euclidean');
iDist = pdist([in; iCenter], 'euclidean');
uDist = pdist([in; uCenter], 'euclidean');

ai = pdist([aCenter; iCenter], 'euclidean');
au = pdist([aCenter; uCenter], 'euclidean');
iu = pdist([iCenter; uCenter], 'euclidean');

certanity = 0;

if (aDist < iDist && aDist < uDist)
    
    if(iDist < uDist)
        certanity = ai/iDist;
    else
         certanity = au/uDist;
    end
    set(handles.textA, 'ForegroundColor', 'green');
    
elseif (iDist < aDist && iDist < uDist)
    
    if(aDist < uDist)
        certanity = ai/aDist;
    else
         certanity = iu/uDist;
    end
    set(handles.textI, 'ForegroundColor', 'green');
    
else 
    
    if(aDist < iDist)
        certanity = au/aDist;
    else
         certanity = iu/iDist;
    end
    set(handles.textU, 'ForegroundColor', 'green');
    
end

certanity = 100*round(certanity, 4);

set(handles.textCertanity, 'String', strcat(num2str(certanity), ''));

