function ClassifyLetter(handles)

addpath UIControls/Classifiers
Clear(handles)

fs = 44100;
dt = 1/fs;
tm = 4;
t = dt : dt : tm;
f = 0 : (1/tm) : fs;

set(handles.btnSpeak, 'visible', 'on')
recorder = audiorecorder(fs, 24, 1);
recordblocking(recorder, tm);
set(handles.btnSpeak, 'visible', 'off')

x = getaudiodata(recorder);

X = abs(fft(x));

fx = sum(X(400:1500)) / sum(X(2500:3000));
fy = (sum(X(4000:5000)) ) / sum(X(1500:2300));

classifier = get(handles.classifierSelection, 'Value');
switch classifier
    case 1
        LetterDecisionTree(handles, fx, fy)
    case 2
        LetterANN(handles, fx, fy)
    case 3
        LettersClustering(handles, fx, fy)
end

pause(0.7)

set(handles.textA, 'ForegroundColor', 'black');
set(handles.textI, 'ForegroundColor', 'black');
set(handles.textU, 'ForegroundColor', 'black');


