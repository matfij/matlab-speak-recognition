function ClassifyWord(handles)

addpath UIControls/Classifiers
Clear(handles)

fs = 44100;
dt = 1/fs;
tm = 4;
t = dt : dt : tm;
f = 0 : (1/tm) : fs;

set(handles.btnSpeak, 'visible', 'on')
recorder = audiorecorder(fs, 24, 1);
recordblocking(recorder, tm);
x = getaudiodata(recorder);
set(handles.btnSpeak, 'visible', 'off')

voice = find(x > 0.07 * max(x));
x = x(voice(1) : voice(end));

win = hamming(200);
nov = 100;
X = abs(spectrogram(x, win, nov));


xfStart = 0.01;
xfEnd = 0.08;
xsStart = 0.6;
xsEnd = 0.7;
yfStart = 0.3;
yfEnd = 0.5;
ysStart = 0.65;
ysEnd = 0.8;

fx = sum(sum(X(floor(xfStart*length(X(:, 1))) : floor(xfEnd*length(X(:, 1))), ...
        floor(xsStart*length(X(1, :))) : floor(xsEnd*length(X(1, :))))));
    
fy = sum(sum(X(floor(yfStart*length(X(:, 1))) : floor(yfEnd*length(X(:, 1))), ...
        floor(ysStart*length(X(1, :))) : floor(ysEnd*length(X(1, :))))));

classifier = get(handles.classifierSelection, 'Value');
switch classifier
    case 1
        WordDecisionTree(handles, fx, fy)
    case 2
        WordANN(handles, fx, fy)
    case 3
        WordClustering(handles, fx, fy)
end
    
pause(0.7)

set(handles.textKot, 'ForegroundColor', 'black');
set(handles.textPies, 'ForegroundColor', 'black');
set(handles.textRyba, 'ForegroundColor', 'black');

